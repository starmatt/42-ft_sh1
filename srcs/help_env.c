/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 16:26:22 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 15:31:42 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

int		setenv_error(char *name, char *value)
{
	if (!name || !value)
	{
		ft_putendl_fd("Usage: setenv [name] [value] (1)", 2);
		return (-1);
	}
	if ((ft_memchr(name, '=', ft_strlen(name))))
		return (cmd_error("setenv", 2));
	return (0);
}

t_env	*def_env(void)
{
	char			*pwd;
	char			buf[1024];
	struct passwd	*user;
	t_env			*env;
	t_env			*new;

	user = getpwuid(getuid());
	pwd = ft_strdup(getcwd(buf, sizeof(buf)));
	env = setup_env("PWD", pwd);
	new = setup_env("OLDPWD", pwd);
	add_env(&env, new);
	new = setup_env("PATH", "/usr/local/bin:/usr/bin:/bin");
	add_env(&env, new);
	new = setup_env("USER", user->pw_name);
	add_env(&env, new);
	new = setup_env("HOME", user->pw_dir);
	add_env(&env, new);
	free(pwd);
	return (env);
}
