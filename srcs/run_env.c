/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_env.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 17:57:37 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 14:44:52 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

static void	free_env(t_env *e)
{
	if (e)
	{
		free(e->env_name);
		free(e->env_value);
		free(e);
	}
}

static void	del_env_2(t_env **e)
{
	t_env	*index;

	if (*e)
	{
		index = (*e)->next;
		free_env(*e);
		*e = index;
	}
}

static void	del_env(char *name, t_env **e)
{
	t_env	*index;
	t_env	*prev;
	t_env	*next;

	index = *e;
	if (index->next && (ft_strcmp(index->env_name, name) != 0))
	{
		prev = index;
		next = prev->next;
		while (next && (ft_strcmp(next->env_name, name) != 0))
		{
			prev = next;
			next = prev->next;
		}
		if (next)
		{
			prev->next = next->next;
			free_env(next);
		}
	}
	else
		del_env_2(e);
}

int			run_unsetenv(char **cmd, t_env **e)
{
	if (!cmd[1])
		return (cmd_error("unsetenv", 4));
	if (cmd[2])
		return (cmd_error("unsetenv", 5));
	if (find_env_item(cmd[1], *e))
		del_env(cmd[1], e);
	else
		return (cmd_error("unsetenv", 6));
	return (0);
}

int			run_setenv(char **cmd, t_env **e)
{
	t_env	*index;

	if (setenv_error(cmd[1], cmd[2]) == -1)
		return (-1);
	if (cmd[2] && cmd[3] && cmd[4])
		return (cmd_error("setenv", 5));
	if ((index = find_env_item(cmd[1], *e)) != NULL)
	{
		if (!cmd[3] || ft_atoi(cmd[3]) == 0)
			return (cmd_error("setenv", 3));
		index->env_value = ft_strdup(cmd[2]);
	}
	else
	{
		index = setup_env(cmd[1], cmd[2]);
		add_env(e, index);
	}
	return (0);
}
