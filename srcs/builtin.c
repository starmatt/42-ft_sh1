/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:53:49 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 16:36:22 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

static void	exit_sh(char **cmd)
{
	if (cmd[1] && cmd[2])
		cmd_error("exit", 5);
	else if (cmd[1])
		exit(ft_atoi(cmd[1]));
	else
		exit(0);
}

static void	print_env(t_env *e)
{
	t_env *index;

	index = e;
	while (index)
	{
		ft_putstr(DEFAULT);
		ft_putstr(index->env_name);
		ft_putchar('=');
		if (index->env_value)
			ft_putendl(index->env_value);
		else
			ft_putchar('\n');
		index = index->next;
	}
}

static void	pretty(void)
{
	int	i;

	i = 100000;
	while (i)
	{
		print_prompt();
		i--;
	}
}

void		run_builtin(int id, char **cmd, t_env **e)
{
	if (id == 1 && !cmd[1])
		print_env(*e);
	else if (id == 2)
		run_setenv(cmd, e);
	else if (id == 3)
		run_unsetenv(cmd, e);
	else if (id == 4)
		ft_putendl("cd does not work yet");
	else if (id == 5)
		exit_sh(cmd);
	else if (id == 6)
		run_echo(cmd, *e);
	else if (id == 7)
		pretty();
}
