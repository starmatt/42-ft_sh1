/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/25 22:55:06 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 14:39:26 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

t_env			*find_env_item(char *name, t_env *e)
{
	t_env	*index;

	index = e;
	while (index)
	{
		if (ft_strcmp(index->env_name, name) == 0)
			break ;
		index = index->next;
	}
	return (index);
}

static void		run_cmd(char **cmd, char **path)
{
	int	i;

	i = 0;
	while (path[i])
	{
		if (execve(ft_strjoin(ft_strjoin(path[i], "/"), cmd[0]),
					cmd, path) == -1)
			i++;
	}
	if (!path[i])
		cmd_error(cmd[0], 1);
	exit(0);
}

static int		check_cmd(char *cmd)
{
	if (ft_strcmp(cmd, "env") == 0)
		return (1);
	else if (ft_strcmp(cmd, "setenv") == 0)
		return (2);
	else if (ft_strcmp(cmd, "unsetenv") == 0)
		return (3);
	else if (ft_strcmp(cmd, "cd") == 0)
		return (4);
	else if (ft_strcmp(cmd, "exit") == 0)
		return (5);
	else if (ft_strcmp(cmd, "echo") == 0)
		return (6);
	else if (ft_strcmp(cmd, "pretty") == 0)
		return (7);
	return (0);
}

static char		**check_line(char *line)
{
	int		i[3];
	char	**cmd;

	i[0] = 0;
	i[1] = 0;
	i[2] = 0;
	while (line[i[0]])
	{
		if (line[i[0]] == '\t')
			i[1]++;
		if (line[i[0]] == ' ')
			i[2]++;
		i[0]++;
	}
	if (i[1] != 0 && i[2] == 0)
	{
		cmd = ft_strsplit(line, '\t');
		cmd = cmd_trim(cmd);
	}
	else
	{
		cmd = ft_strsplit(line, ' ');
		cmd = cmd_trim(cmd);
	}
	return (cmd);
}

void			run(char *line, t_env **e)
{
	pid_t	pid;
	t_env	*index;
	char	**cmd;
	char	**path;
	int		n;

	if ((index = find_env_item("PATH", *e)))
		path = ft_strsplit(index->env_value, ':');
	cmd = check_line(line);
	if ((n = check_cmd(cmd[0])) != 0)
		run_builtin(n, cmd, e);
	else
	{
		signals2();
		pid = fork();
		if (pid > 0)
			wait(&pid);
		else if (pid == 0)
			run_cmd(cmd, path);
	}
}
