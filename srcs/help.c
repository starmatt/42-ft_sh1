/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:19:58 by mborde            #+#    #+#             */
/*   Updated: 2015/03/24 18:08:20 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

int			cmd_error(char *cmd, int id)
{
	if (id == 1)
		ft_putendl_fd(ft_strjoin("ft_minishell1: command not found: ", cmd), 2);
	else if (id == 2)
		ft_putendl_fd(ft_strjoin(cmd, ": [name] cannot contain '='"), 2);
	else if (id == 3)
		ft_putendl_fd(ft_strjoin(cmd, ": cannot overwrite env value"), 2);
	else if (id == 4)
		ft_putendl_fd(ft_strjoin(cmd, ": not enough arguments"), 2);
	else if (id == 5)
		ft_putendl_fd(ft_strjoin(cmd, ": too many arguments"), 2);
	else if (id == 6)
		ft_putendl_fd(ft_strjoin(cmd, ": environment variable not found"), 2);
	return (-1);
}

static void	cmd_trim_2(char **cmd)
{
	int	i;

	i = 0;
	while (cmd[i])
	{
		cmd[i] = ft_strtrim(cmd[i]);
		i++;
	}
	cmd[i] = NULL;
}

char		**cmd_trim(char **cmd)
{
	int	i;
	int	len;
	int	j;

	len = 0;
	while (cmd[len])
		len++;
	cmd_trim_2(cmd);
	i = 0;
	j = 0;
	while (i < len)
	{
		if (cmd[i][0] != 0)
		{
			cmd[j] = cmd[i];
			j++;
		}
		i++;
	}
	cmd[j] = NULL;
	return (cmd);
}

char		*del_dblchr(char *s, char c)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (s[i] == c && s[i + 1] && s[i + 1] == c)
		{
			s[i] = 0;
			ft_strcat(s, &s[i + 1]);
			i--;
		}
		i++;
	}
	return (s);
}

int			is_line_empty(char *line)
{
	size_t	i;

	i = 0;
	while (ft_isblank(line[i]) == 1)
		i++;
	if (i == ft_strlen(line))
		return (0);
	return (1);
}
