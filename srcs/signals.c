/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/18 15:55:32 by mborde            #+#    #+#             */
/*   Updated: 2015/03/23 17:51:25 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

static void	signal_action(int sign)
{
	(void)sign;
	ft_putendl(CLEAR);
	print_prompt();
}

static void	signal_action_2(int sign)
{
	(void)sign;
	ft_putstr(CLEAR);
}

void		signals(void)
{
	signal(SIGINT, signal_action);
	signal(SIGTSTP, signal_action_2);
	signal(SIGQUIT, signal_action_2);
}

void		signals2(void)
{
	signal(SIGINT, signal_action_2);
	signal(SIGTSTP, signal_action_2);
	signal(SIGQUIT, signal_action_2);
}
