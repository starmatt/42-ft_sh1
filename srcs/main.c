/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 11:12:52 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 14:38:32 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

void			add_env(t_env **env, t_env *new)
{
	t_env	*index;

	index = *env;
	if (*env)
	{
		while (index->next)
			index = index->next;
		index->next = new;
	}
	else
		*env = new;
}

t_env			*setup_env(const char *name, const char *value)
{
	t_env	*index;

	if (!(index = (t_env *)malloc(sizeof(t_env))))
		return (NULL);
	index->env_name = ft_strdup(name);
	if (!value)
		index->env_value = NULL;
	else
		index->env_value = ft_strdup(value);
	index->next = NULL;
	return (index);
}

t_env			*get_env(char **env)
{
	t_env	*index;
	t_env	*e;
	int		i;
	char	**split_e;

	i = 0;
	index = NULL;
	e = NULL;
	while (env[i])
	{
		split_e = ft_strsplit(env[i], '=');
		if (i == 0)
			index = setup_env(split_e[0], split_e[1]);
		else
		{
			e = setup_env(split_e[0], split_e[1]);
			add_env(&index, e);
		}
		i++;
	}
	return (index);
}

static void		before_run(char *line, t_env **e)
{
	int		i;
	char	**split_line;

	i = 0;
	split_line = ft_strsplit(line, ';');
	while (split_line[i])
	{
		if (is_line_empty(split_line[i]) == 1)
			run(split_line[i], e);
		i++;
	}
}

int				main(int argc, char **argv, char **env)
{
	char	*line;
	int		gnl;
	t_env	*e;

	if (env[0])
		e = get_env(env);
	else
		e = def_env();
	while (argc && argv[0])
	{
		signals();
		line = NULL;
		print_prompt();
		if ((gnl = get_next_line(0, &line)) > 0 && ft_strlen(line))
		{
			if (is_line_empty(line) == 1)
				before_run(line, &e);
			free(line);
		}
		else if (gnl == 0)
			return (0);
	}
	return (0);
}
