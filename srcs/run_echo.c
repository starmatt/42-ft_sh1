/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_echo.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/18 13:33:24 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 15:18:20 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

static void	run_dollar(char **cmd, int i, int nflag, t_env *e)
{
	char	**split;
	t_env	*index;

	split = ft_strsplit(cmd[i], '$');
	if (cmd[i + 1])
	{
		if ((index = find_env_item(split[0], e)))
		{
			ft_putstr(index->env_value);
			ft_putchar(' ');
		}
	}
	else
	{
		if ((index = find_env_item(split[0], e)) && nflag == 0)
			ft_putendl(index->env_value);
		else if ((index = find_env_item(split[0], e)) && nflag == 1)
			ft_putstr(index->env_value);
	}
}

static void	run_minusn(char **cmd, t_env *e)
{
	int	i;

	if (cmd[2])
	{
		i = 2;
		while (cmd[i])
		{
			if (cmd[i][0] == '$' && cmd[i][1])
				run_dollar(cmd, i, 1, e);
			else
			{
				if (cmd[i + 1])
				{
					ft_putstr(cmd[i]);
					ft_putchar(' ');
				}
				else
					ft_putstr(cmd[i]);
			}
			i++;
		}
	}
}

static void	run_noop(char **cmd, int i)
{
	if (cmd[i + 1])
	{
		ft_putstr(cmd[i]);
		ft_putchar(' ');
	}
	else
		ft_putendl(cmd[i]);
}

void		run_echo(char **cmd, t_env *e)
{
	int	i[2];

	i[0] = 0;
	i[1] = 1;
	if (!cmd[1])
		ft_putchar('\n');
	else
	{
		if (ft_strcmp(cmd[1], "-n") != 0)
		{
			while (cmd[i[1]])
			{
				if (cmd[i[1]][0] == '$' && cmd[i[1]][1])
					run_dollar(cmd, i[1], 0, e);
				else
					run_noop(cmd, i[1]);
				i[1]++;
			}
		}
		else if (ft_strcmp(cmd[1], "-n") == 0)
			run_minusn(cmd, e);
		i[0]++;
	}
}
