/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/04 12:49:11 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 14:18:43 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

static char		*get_pwd_2(char **pwd, int i)
{
	char	*get_pwd;

	get_pwd = ft_strjoin("~/", pwd[i]);
	while (pwd[i + 1])
	{
		if (pwd[i + 1])
			get_pwd = ft_strjoin(ft_strjoin(get_pwd, "/"), pwd[i + 1]);
		else
			get_pwd = ft_strjoin(ft_strjoin(get_pwd, "/"), pwd[i]);
		i++;
	}
	return (get_pwd);
}

static char		*get_pwd(void)
{
	char	buf[1024];
	char	**split_pwd;
	int		i;

	i = 0;
	split_pwd = ft_strsplit(getcwd(buf, sizeof(buf)), '/');
	while (split_pwd[i])
	{
		if (ft_strcmp(split_pwd[i], getpwuid(getuid())->pw_name) == 0)
		{
			if (!(split_pwd[i + 1]))
				return ("~");
			return (get_pwd_2(split_pwd, i + 1));
		}
		i++;
	}
	return (getcwd(buf, sizeof(buf)));
}

void			print_prompt(void)
{
	ft_putstr_fd(GREEN, 0);
	ft_putstr_fd(getpwuid(getuid())->pw_name, 0);
	ft_putstr_fd(WHITE, 0);
	ft_putchar_fd('@', 0);
	ft_putstr_fd(MAGENTA, 0);
	ft_putstr_fd(get_pwd(), 0);
	ft_putstr_fd(DEFAULT, 0);
	ft_putstr_fd("> ", 0);
}
