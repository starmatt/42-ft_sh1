/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 01:38:33 by mborde            #+#    #+#             */
/*   Updated: 2015/02/08 11:39:49 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "libft.h"
# define BUFF_SIZE 64

int				get_next_line(const int fd, char **line);
typedef struct	s_file
{
	int				fd;
	char			*buff;
	struct s_file	*next;
}				t_file;

#endif
