/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh1.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 11:28:20 by mborde            #+#    #+#             */
/*   Updated: 2015/03/25 14:45:11 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SH1_H
# define FT_SH1_H
# include "libft.h"
# include "get_next_line.h"
# include <unistd.h>
# include <stdlib.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <dirent.h>
# include <signal.h>
# define CLEAR "\b\b\e[0J"
# define GREEN "\e[32m"
# define WHITE "\e[97m"
# define MAGENTA "\e[35m"
# define DEFAULT "\e[0m"

typedef struct	s_env
{
	char			*env_name;
	char			*env_value;
	struct s_env	*next;
}				t_env;

void			print_prompt(void);
int				is_line_empty(char *line);
t_env			*get_env(char **env);
t_env			*setup_env(const char *name, const char *value);
void			add_env(t_env **env, t_env *new);
t_env			*def_env(void);
void			run(char *line, t_env **e);
void			run_builtin(int n, char **cmd, t_env **e);
int				run_setenv(char **cmd, t_env **e);
int				run_unsetenv(char **cmd, t_env **e);
int				setenv_error(char *name, char *value);
char			**cmd_trim(char **cmd);
int				cmd_error(char *cmd, int id);
t_env			*find_env_item(char *name, t_env *e);
int				mod_env_value(char **cmd, t_env **env);
void			run_echo(char **cmd, t_env *e);
void			signals(void);
void			signals2(void);

#endif
