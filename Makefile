# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mborde <mborde@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/10 10:52:33 by mborde            #+#    #+#              #
#    Updatd: 2015/02/10 12:05:05 by mborde           ###   ########.fr         #
#                                                                              #
# **************************************************************************** #

NAME = ft_minishell1

SRCS = srcs/main.c			\
	   srcs/get_next_line.c	\
	   srcs/prompt.c		\
	   srcs/signals.c		\
	   srcs/run.c			\
	   srcs/builtin.c		\
	   srcs/run_env.c		\
	   srcs/help_env.c		\
	   srcs/run_echo.c		\
	   srcs/help.c			\
	   libft/libft.a

INCS = -I incs/

OBJS = $(SRCS":.c=.o")

FLAG = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
ifneq ("$(wildcard ./libft/libft.a)","")
	@echo "Compiling ft_sh1..."
	@gcc $(FLAG) -o $(NAME) $(SRCS) $(INCS)
	@echo "	OK"
else
	@make libre
	@echo "Compiling ft_sh1..."
	@gcc $(FLAG) -o $(NAME) $(SRCS) $(INCS)
	@echo "	OK"
endif

clean:
	@echo "Removing object files."
	@/bin/rm -f $(OBJS)

fclean: clean
	@echo "Removing binary."
	@/bin/rm -f $(NAME)

libre:
	@echo "Rebuilding libft..."
	@make -C libft/ re
	@echo "	OK"

libfc:
	@make -C libft/ fclean
	@echo "	OK"

re: fclean all
