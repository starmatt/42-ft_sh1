/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 12:39:07 by mborde            #+#    #+#             */
/*   Updated: 2014/11/28 21:35:32 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	char	*t;

	if (b == NULL)
		return (NULL);
	t = b;
	while (len)
	{
		*t = c;
		t++;
		len--;
	}
	return (b);
}
