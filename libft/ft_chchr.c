/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chchr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 08:47:05 by mborde            #+#    #+#             */
/*   Updated: 2015/03/16 09:12:41 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_chchr(const char *s, char *change_from, char *change_to)
{
	int		i;
	int		len;
	char	*result;
	char	*cpy;
	char	*tmp[2];

	i = 0;
	result = NULL;
	cpy = ft_strdup(s);
	if ((tmp[0] = ft_strstr(cpy, change_from)))
	{
		len = ft_strlen(s) - ft_strlen(change_from) + ft_strlen(change_to) + 1;
		result = ft_strnew(len);
		while (i < (int)ft_strlen(change_from))
			i++;
		tmp[1] = &tmp[0][i];
		tmp[0][0] = 0;
		ft_strcat(result, cpy);
		ft_strcat(result, change_to);
		ft_strcat(result, tmp[1]);
	}
	return (result);
}
